# LaTeX Tutorial

Mein Versuch eines LaTeX-Tutorials, noch offen, welche Form das annehmen wird.

## Lizenz

Dieses Werk ist lizenziert unter einer Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz.

[![CC BY-NC-SA 4.0](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

### Zusammenfassung

Dies ist eine allgemeinverständliche Zusammenfassung der Lizenz (die diese nicht ersetzt):

#### Sie dürfen

<dl>
  <dt>Teilen</dt>
  <dd>das Material in jedwedem Format oder Medium vervielfältigen und weiterverbreiten</dd>

  <dt>Bearbeiten</dt>
  <dd>das Material remixen, verändern und darauf aufbauen</dd>
</dl>

Der Lizenzgeber kann diese Freiheiten nicht widerrufen solange Sie sich an die Lizenzbedingungen halten.

#### Unter folgenden Bedingungen

<dl>
  <dt>Namensnennung</dt>
  <dd>Sie müssen angemessene Urheber- und Rechteangaben machen, einen Link zur Lizenz beifügen und angeben, ob Änderungen vorgenommen wurden. Diese Angaben dürfen in jeder angemessenen Art und Weise gemacht werden, allerdings nicht so, dass der Eindruck entsteht, der Lizenzgeber unterstütze gerade Sie oder Ihre Nutzung besonders.</dd>

  <dt>Nicht kommerziell</dt>
  <dd>Sie dürfen das Material nicht für kommerzielle Zwecke nutzen.</dd>


  <dt>Weitergabe unter gleichen Bedingungen</dt>
  <dd>Wenn Sie das Material remixen, verändern oder anderweitig direkt darauf aufbauen, dürfen Sie Ihre Beiträge nur unter derselben Lizenz wie das Original verbreiten.</dd>


  <dt>Keine weiteren Einschränkungen</dt>
  <dd>Sie dürfen keine zusätzlichen Klauseln oder technische Verfahren einsetzen, die anderen rechtlich irgendetwas untersagen, was die Lizenz erlaubt.</dd>
</dl>
